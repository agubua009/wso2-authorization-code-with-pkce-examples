# Ejemplos WSO2 IS 
Esta carpeta incluye un set de ejemplos que demuestran el potencial de WSO2 IS.

[AuthorizationCodeWithPKCEAngular](https://gitlab.com/agubua009/wso2-authorization-code-with-pkce-examples/tree/master/AuthorizationCodeWithPKCEAngular)
* Es un ejemplo de un sitio web desarrollado en angular y que demuestra el funcionamiento de Oauth2 de WSO2 mediante Authorization Code con PKCE

[AuthorizationCodeWithPKCEIonic](https://gitlab.com/agubua009/wso2-authorization-code-with-pkce-examples/tree/master/AuthorizationCodeWithPKCEIonicApp)
* Es un ejemplo de una app desarrollada en ionic y que demuestra el funcionamiento de Oauth2 de WSO2 mediante Authorization Code con PKCE