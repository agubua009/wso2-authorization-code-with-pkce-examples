import { Component } from '@angular/core';
import { User } from '../services/authentication.service';
import { UsersService } from '../services/usersservice';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  user: User;
  constructor(private users: UsersService) {

    this.user = users.getUser();

  }

}
