import { Component } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage {

  constructor(private authService: AuthenticationService) {

   }


  onLogin() {
    this.authService.initAuthorizationCodeFlow();
  }
}
