import { Injectable } from '@angular/core';
import { User } from './authentication.service';

@Injectable()
export class UsersService {
    user: User;
    constructor() {
    }
    getUser() {
        this.user = this._getUser();
        return this.user;
    }
    getAuthorName() {
        this.user = this._getUser();
        return `${this.user.name} <${this.user.email}>`;
    }
    private _getUser() {
        try {
            const sus = localStorage.getItem('currentUser');
            if (sus == null) {
                return null;
            }
            const us = JSON.parse(sus) as User;
            return us;
        } catch (ex) {
            return null;
        }
    }
}