import { filter, map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
export interface BroadcastEvent {
  key: any;
  data?: any;
}

@Injectable()
export class Events {
  private eventBus: Subject<BroadcastEvent>;

  constructor() {
    this.eventBus = new Subject<BroadcastEvent>();
  }

  broadcast(key: any, data?: any) {
    this.eventBus.next({ key, data });
  }

  on<T>(key: any): Observable<T> {
    return this.eventBus.asObservable().pipe(filter(event => event.key === key), map(event => event.data as T));
  }
}
