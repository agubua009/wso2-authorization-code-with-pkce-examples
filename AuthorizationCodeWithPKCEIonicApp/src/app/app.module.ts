import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthenticationService } from './services/authentication.service';
import { OAuthModule, OAuthService } from 'angular-oauth2-oidc-codeflow-pkce';
import { HttpClientModule } from '@angular/common/http';
import { Events } from './services/events';
import { UsersService } from './services/usersservice';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    OAuthModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Events,
    InAppBrowser,
    UsersService,
    AuthenticationService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    OAuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
