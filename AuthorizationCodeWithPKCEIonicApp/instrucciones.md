# Instrucciones para solucionar problemas con crypto

## Abrir el archivo:
	\.node_modules\@angular-devkit\build-angular\src\angular-cli-files\models\webpack-configs\browser.js

	modificar 
	node: false
	node: { crypto: true, stream: true, buffer: true }


# Instrucciones para solucionar problemas con paginas con ssl invalido

## Abrir el archivo:
	.\plugins\cordova-plugin-inappbrowser\src\android\InAppBrowser.java

## Agregar los siguientes imports:

	import android.net.http.SslError;
	import android.webkit.SslErrorHandler;


	y luego el siguiente metodo

	public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
		handler.proceed();
		return;
	}

	Ejecutar los siguientes comandos

	ionic cordova platform remove android
	ionic cordova platform add android




# Para cambiar el IS o APIM que se usa modificar en 
src\assets\constants.js

issuer y clientId