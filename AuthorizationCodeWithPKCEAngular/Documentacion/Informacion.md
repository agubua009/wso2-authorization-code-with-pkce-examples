# INFORMACION
### WSO2 Identity Server-5.7.0
* Crear Service Provider
* Habilitar acceso mediante OAUTH2/OIDC
* Crear clientId y clientSecret
* Agregar claims de email, fullname y role
* Habilitar PKCE y que no solicite el clientSecret desde la configuracion de OAUTH/OIDC
* 	 para configurar las url oauth2 de acceso debemos cambiar los archivos de configuracion /opt/wso2/wso2is-km-5.7.0/repository/conf/identity/application-authentication.xml
 * Buscar AuthenticationEndpointURL, AuthenticationEndpointRetryURL, AuthenticationEndpointMissingClaimsURL y cambiar 
			/authenticationendpoint/xxxxxx por https://my-is-server.com/authenticationendpoint/xxxxxx
 * Buscar /opt/wso2/wso2is-km-5.7.0/repository/conf/identity/identity.xml esta lleno de URL donde hay que cambiar por ejemplo 
			<OpenIDLoginUrl>${carbon.protocol}://${carbon.host}:${carbon.management.port}/authenticationendpoint/openid_login.do</OpenIDLoginUrl> y cambiar por
			<OpenIDLoginUrl>https://my-is-server.com/authenticationendpoint/openid_login.do</OpenIDLoginUrl>
	- Para el acceso a CORS debemos agregar al archivo <PRODUCT_HOME>/repository/deployment/server/webapps/oauth2/WEB-INF/web.xml
		<filter>
			<filter-name>CORS</filter-name>
			<filter-class>com.thetransactioncompany.cors.CORSFilter</filter-class>
			<init-param>
				<param-name>cors.allowOrigin</param-name>
				<param-value>http://example.com</param-value>
			</init-param>
		</filter>
		 
		<filter-mapping>
			<filter-name>CORS</filter-name>
			<url-pattern>/example.html</url-pattern>
		</filter-mapping>
 		donde **http://example.com** son los dominios habilitados para el acceso, por ejemplo * 
        y **/example.html** las paginas habilitadas, por ejemplo *

### ANGULAR
* Crear proyecto angular e instalar
	 * `npm i angular-oauth2-oidc-codeflow-pkce --save`
	 * edit node_modules/@angular-devkit/build-angular/src/angular-cli-files/models/webpack-configs/browser.js

         `node: false, ==> node: { crypto: true, stream: true }`
	 * edit src/assets/constants.js con lo datos de acceso del IS
		
# EJEMPLOS DE RESPUESTA QUE SE PUEDEN OBTENER:
    https://localhost:9443/oauth2/authorize?response_type=code&client_id=XALwgcRGsR4zud4RsokMmtNm3xQa&redirect_uri=https://localhost/callback&scope=read

    https://my-is-server.com:9443/oauth2/authorize?response_type=code&client_id=jP81FM0efuREqswtg4ZsdDQurPga&redirect_uri=http://localhost:4200/callback&scope=openid&code_challenge=OTU5ODZhODlkMjgxZGRlNzc3MWQ0YTc0NGJjY2RhZmUxYzgyNjg3MDc0NTI3NzY3OTdlYjE4NjRkNjU4ODQzOA&code_challenge_method=S256

    https://my-is-server.com:9443/oauth2/authorize?response_type=code&client_id=jP81FM0efuREqswtg4ZsdDQurPga&redirect_uri=http://localhost:4200/callback&scope=openid&code_challenge=d2bf3422b143a0a97174fc8721a856080f7e79d06bb3c89fb7173086a8258f8d&code_challenge_method=S256

    https://my-is-server.com:9443/oauth2/authorize?response_type=code&client_id=jP81FM0efuREqswtg4ZsdDQurPga&state=VitjNLsVdk2Bg1Zk6fl3Mr4zE8gjaMKAF1lZvF8V&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2Fcallback&scope=openid%20profile&nonce=VitjNLsVdk2Bg1Zk6fl3Mr4zE8gjaMKAF1lZvF8V&code_challenge=ODdmMGI0Mjk1YzRmODMyMDlmNzJhYmVkYWY0MjlhYjM2YjgzNTdhYTI2ZDI3ODVlNjI0MDkzYTVmNWNhYzYzZg&code_challenge_method=S256

    https://localhost:9443/authenticationendpoint/oauth2_consent.do?loggedInUser=vatrox03&application=vatrox_sp&tenantDomain=carbon.super&scope=openid+profile&sessionDataKeyConsent=187137ce-dfed-4a0e-8f26-3ea376d58ff2&spQueryParams=response_type%3Dcode%26client_id%3DjP81FM0efuREqswtg4ZsdDQurPga%26state%3DWxv4Dfl3loed0qU27Ui6q1EgfqUdSlXE6L5agOnw%26redirect_uri%3Dhttp%253A%252F%252Flocalhost%253A4200%252Fcallback%26scope%3Dopenid%2520profile%26nonce%3DWxv4Dfl3loed0qU27Ui6q1EgfqUdSlXE6L5agOnw%26code_challenge%3DODdmMGI0Mjk1YzRmODMyMDlmNzJhYmVkYWY0MjlhYjM2YjgzNTdhYTI2ZDI3ODVlNjI0MDkzYTVmNWNhYzYzZg&mandatoryClaims=0_Full+Name%2C1_Username%2C2_Email%2C3_Role

    http://localhost:4200/login?returnUrl=%2Fcallback%3Fcode%3Dab6fbd73-a074-3de4-ba12-e8076e14b645%26state%3DWxv4Dfl3loed0qU27Ui6q1EgfqUdSlXE6L5agOnw%26session_state%3D6456262bf569d7e5de78d2818588ae6b295ee18e71aaf199e639d30554be8680.-uSW-r6cR8CRo9cHnWXJSA

    http://localhost:4200/callback?code=b30f4c26-da94-3709-ab10-7e33a0d8015c&state=VrGkylZjkGjLQHTbdeCPjYCsvJnu1OqKH6Bz8fnl&session_state=28de512612d9b22a2b5fea23ced791c938e453cb37757f8f306f85fb76e5f300.p8iwt2KljOsuEiDzpIfrGg



-----


# REFERENCIAS:
* [Mobile Apps](https://aaronparecki.com/oauth-2-simplified/#mobile-apps)
* [Oidc Angular](https://github.com/bipiane/angular-oauth2-oidc)
* [Cors](https://docs.wso2.com/display/IS570/Invoking+an+Endpoint+from+a+Different+Domain) 
* [Info PKCE 1](https://medium.com/@inthiraj1994/pkce-in-wso2-is-server-51a7eeed2d19)
* [Info PKCE 2](https://auth0.com/docs/api-auth/tutorials/authorization-code-grant-pkce)
* [Info PKCE 3](https://developers.onelogin.com/openid-connect/guides/auth-flow-pkce)
* [Info Para versiones de Wso2 inferiores a la 5.7.0](https://github.com/mefarazath/Authorization-Grant-Without-Client-Secret)
* [Info Wso2 Authorization Code](https://docs.wso2.com/display/IS450/Authorization+Code+Grant+Type+with+API+Manager)
* [Info Wso2](https://wso2.com/library/articles/2014/02/securing-your-web-service-with-oauth2-using-wso2-identity-server-1/)
* [Info Preservar estado con cookies](https://manfredsteyer.github.io/angular-oauth2-oidc/docs/additional-documentation/preserving-state-(like-the-requested-url).html)




# PROBLEMAS:
* Detecte lo siguiente, cuando una persona realiza un logout queda almanecada una cookie sobre el navegador, y eso hace que al intentar volver a loguarse hace todo automaticamente sin preguntar el usuario, deberiamos buscar la forma de deshabilitar esa cookie.
    [Encontre esto al respecto](https://docs.wso2.com/display/IS570/Authentication+Session+Persistence)