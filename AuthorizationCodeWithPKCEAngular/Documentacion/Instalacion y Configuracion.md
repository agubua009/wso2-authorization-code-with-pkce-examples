# Instalacion:
### Requisitos: 
* NodeJS
	
### Comandos Prueba:
* En la carpeta del proyecto ejecutar: `npm i`
* Verficar que no existan errores
* Ejecutar `ng serve`
* Ingresar en un browser a (http://localhost:4200)
		
### Comandos Compilacion:
* En la carpeta del proyecto ejecutar: `ng build --prod`
* Verficar que no existan errores
* Esto generará una carpeta dist en la carpeta del proyecto, dentro existira una carpeta que contiene el codigo compilado para llevar al servidor
		
	
### Configuraciones Necesarias:
Para publicar el sitio en un servidor, debemos configurar los siguientes archivos
* index.html => cambiar el tag <base href="/pkceexample/"> por la seccion virtual de la ruta en donde se aloja el sitio
* assets/constants.js => Existen varios parametros a configurar
    * **issuer**: Url de acceso del servidor de Oauth2. Ej: 'https://my-is-server.com:9443/oauth2',
    * **redirectUri**: Url de callback debe conicidir con la configurada en el service provider configurado en el servidor de Oauth2. Ej: 'https://my-angular-domain.com/pkceexample/callback',
    * **clientId**: ClientId del service provider creado para su aplicacion este se encuentra en servidor de Oauth2 en la seccion Oauth2/OIDC. Ej: '234535345de35d35a3434b34a34',
    * **scope**: Scopes que le solicitamos. Ej: 'openid profile email'
