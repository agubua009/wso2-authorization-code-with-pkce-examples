# AuthorizationCodeWithPKCEAngular

Es un ejemplo de un sitio web desarrollado en angular y que demuestra el funcionamiento de Oauth2 de WSO2 mediante Authorization Code con PKCE

#### Entorno de prueba:
* [https://my-angular-domain.com/pkceexample/](https://my-angular-domain.com/pkceexample/)
    * Debe iniciar sesión con algun usuario del Ldap de la caja y autorizar la lectura de los claims.
    * Una ves loqueado podra visualizar los datos del usuario 
        * Email
        * Nombre
        * Roles

#### Links a documentación:
* [Instalación y Configuración](https://gitlab.com/agubua009/wso2-authorization-code-with-pkce-examples/blob/master/AuthorizationCodeWithPKCEAngular/Documentacion/Instalacion%20y%20Configuracion.md)
* [Información](https://gitlab.com/agubua009/wso2-authorization-code-with-pkce-examples/blob/master/AuthorizationCodeWithPKCEAngular/Documentacion/Informacion.md)
* [Archivo de POSTMAN de ejemplos](https://gitlab.com/agubua009/wso2-authorization-code-with-pkce-examples/blob/master/AuthorizationCodeWithPKCEAngular/Documentacion/Ejemplos%20POSTMAN/WSO2.postman_collection.json)

