var constants = {
    issuer: 'https://[URL_APIM:PORT_APIM]/oauth2',
    redirectUri: '[DOMAIN_OF_MY_ANGUlAR_APP]/pkceexample/callback',
    clientId: '[CLIENT_ID_OF_WSO2_APIM_APPLICATION]',
    scope: 'openid profile email'
};
window["constants"] = constants;