import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    //styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

    mobileMenuActive: boolean;
    displayDialog: boolean;
    constructor() { }

    ngOnInit() {
        
    }

    onMobileMenuButton(event) {
        this.mobileMenuActive = !this.mobileMenuActive;
        event.preventDefault();
    }

    hideMobileMenu(event) {
        this.mobileMenuActive = false;
        event.preventDefault();
    }
}
