﻿import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Events } from 'src/app/services/events';
import { Router } from '@angular/router';

@Component({ templateUrl: 'nopermissions.component.html' })
export class NoPermissionsComponent implements OnInit {
    constructor(
        private authenticationService: AuthenticationService, private events: Events, private router: Router) { }

    ngOnInit() {
        this.events.broadcast("onChangeStateTopBar", false);
    }

    exit() {
        this.router.navigate(['/login']);
        this.authenticationService.logout(true);
    }
}
