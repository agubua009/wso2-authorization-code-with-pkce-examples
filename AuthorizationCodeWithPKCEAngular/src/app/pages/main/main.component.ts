import { Component, OnInit, NgZone, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { DialogService } from 'primeng/api';
import { UsersService } from '../../services/usersservice';
import * as moment from 'moment';
import * as linq from 'linqts';
import * as $ from 'jquery';
import { User } from 'src/app/services/authentication.service';

@Component({
    templateUrl: './main.component.html'
})
export class MainComponent implements OnInit, OnDestroy {

    user: User;

    constructor(private zone: NgZone, private chRef: ChangeDetectorRef, private usersService: UsersService, 
        public dialogService: DialogService) { }

    ngOnInit() {
        var self = this;
        self.user = self.usersService.getUser();
        $("#style-boostrap").remove();
    }

    ngOnDestroy(): void {
        
    }

    private runInUiThread(func) {
        this.zone.run(() => {
            func();
            this.chRef.markForCheck();
        });
    }
}
