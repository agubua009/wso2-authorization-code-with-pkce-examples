﻿import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({ templateUrl: 'callback.component.html' })
export class CallBackComponent implements OnInit {
    constructor(private authenticationService: AuthenticationService, private route: ActivatedRoute, private router: Router) {
        route.queryParamMap.subscribe((ps => {
            if(ps == null || ps.keys == null || ps.keys.length == 0)
                return this.redirectLogin();
            for (var i in ps.keys) {
                var name = ps.keys[i];
                console.log(name + " = " + ps.get(name));
            }
            var code = ps.get("code");
            if(code == null || code.length == 0)
                return this.redirectLogin();
            
            this.authenticationService.processAutorizationCode(code);
        }));
    }

    ngOnInit() {

    }

    private redirectLogin(){
        this.router.navigate(['/login']);
    }


}
