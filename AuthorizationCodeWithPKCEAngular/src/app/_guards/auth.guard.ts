﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UsersService } from '../services/usersservice';
import * as $ from 'jquery';
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private usersService: UsersService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        var user = this.usersService.getUser();
        if (user == null) {
            if($('#style-boostrap').length == 0)
                $("head").append('<link id="style-boostrap" href="assets/bootstrap.min.css" rel="stylesheet" />');
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            return false;
        }
        return true;
    }
}