import { Component, Pipe, PipeTransform, OnInit, OnDestroy, NgZone, ChangeDetectorRef } from '@angular/core';
import { AppComponent } from './app.component';
import * as $ from 'jquery';
import * as linq from 'linqts';
import { CommonService } from './services/commonservice';
import { UsersService } from './services/usersservice';
import { User } from './services/authentication.service';
import { Utils } from './services/utils';
import { Events } from './services/events';
import { Subscription } from 'rxjs';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent implements OnInit, OnDestroy {

  menuOpen: boolean = false;
  _user: User;

  subscription: Subscription;
  interval = null;
  prevCount: number = 0;

  constructor(public app: AppComponent, private commonsService: CommonService,
    private usersService: UsersService, private utils: Utils, private events: Events,
    private authService: AuthenticationService,
    private zone: NgZone, private chRef: ChangeDetectorRef) {
    this._user = usersService.getUser();
  }

  ngOnInit() {
    var self = this;
    self.subscription = self.events.on("onUserStateChanged").subscribe(() => {
      self.runInUiThread(() => {
        self._user = self.usersService.getUser();
      })
    });

  }

  ngOnDestroy() {
    if (this.subscription != undefined)
      this.subscription.unsubscribe();
    if (this.interval){
      clearInterval(this.interval);
      this.interval = null;
    }
      
  }


  logout() {
    this.authService.logout(true);
  }

  logoutRt() {
    this.authService.logoutWithRevokeToken();
  }


  runInUiThread(func) {
    this.zone.run(() => {
      func();
      this.chRef.markForCheck();
    });
  }



  toggleMenu() {
    this.menuOpen = !this.menuOpen;
  }

  closeMenu() {
    this.menuOpen = false;
  }
}

