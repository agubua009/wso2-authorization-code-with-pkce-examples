export class Wso2Configuration {
  issuer: string;
  redirectUri: string;
  clientId: string;
  scope: string;
}

export const authConfig : Wso2Configuration = window["constants"];