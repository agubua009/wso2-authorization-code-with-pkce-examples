import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { Events } from './events';
import * as moment from 'moment';
import { ConfirmationService } from 'primeng/api';

@Injectable()
export class Utils {

    constructor(private messageService: MessageService, private events: Events, private confirmationService: ConfirmationService) {
    }

    public showSuccess(title, message, duration: number = 2500) {
        this.messageService.add({ key: 'top-right', severity: 'success', summary: title, detail: message, life: duration });
    }

    
    public showSuccessHtml(title, message, duration: number = 2500) {
        this.messageService.add({ key: 'top-right-html', severity: 'success', summary: title, detail: message, life: duration });
    }

    public showWarn(title, ex, duration: number = 2500) {
        if (ex != undefined && ex.error != undefined && ex.error.reason != undefined) {
            this.messageService.add({ key: 'top-right', severity: 'warn', summary: title, detail: ex.error.reason, life: duration });
        }
        else {
            this.messageService.add({ key: 'top-right', severity: 'warn', summary: title, detail: ex, life: duration });
        }
    }

    public showError(title, ex, duration: number = 2500) {
        if (ex != undefined && ex.error != undefined && ex.error.reason != undefined) {
            this.messageService.add({ key: 'top-right', severity: 'error', summary: title, detail: ex.error.reason, life: duration });
        }
        else if(ex != undefined && ex.message != undefined ) {
            this.messageService.add({ key: 'top-right', severity: 'error', summary: title, detail: ex.message, life: duration });
        }
        else if(ex != undefined && typeof(ex) == "object") {
            console.error(ex);
            this.messageService.add({ key: 'top-right', severity: 'error', summary: title, detail: JSON.stringify(ex), life: duration });
        }
        else {            
            this.messageService.add({ key: 'top-right', severity: 'error', summary: title, detail: ex, life: duration });
        }
    }

    public showAlert(title, text, accept, reject?) {
        this.confirmationService.confirm({
            message: text,
            header: title,

            icon: 'pi pi-info-circle',
            accept: () => {
                if (accept) accept();
            },
            reject: () => {
                if (reject) reject();
            }
        });
    }


    public showLoading() {
        this.events.broadcast("loading.show");
    }

    public hiseLoading() {
        this.events.broadcast("loading.hide");
    }



    public getFistProperty(obj) {
        for (var i in obj)
            return obj[i];
        return null;
    }


    public convertToDate(stringDate: string) {
        return moment(stringDate, "DD/MM/yy 00:00:00").toDate();
    }

    public copyObject(src) {
        return Object.assign({}, src);
    }

    public convertGeoPoitToString(geo: any) {
        if (geo == null)
            return "";
        return geo._lat + ", " + geo._long;
    }
    public getCalendarFormat() {
        return {
            firstDayOfWeek: 1,
            dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
            dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
            dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
            monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
            monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
            today: 'Hoy',
            clear: 'Borrar'
        };
    }
}