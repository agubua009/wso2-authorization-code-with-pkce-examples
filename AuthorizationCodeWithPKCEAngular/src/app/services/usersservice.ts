import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './authentication.service';
import { Events } from './events';

@Injectable()
export class UsersService {
    _user: User;
    constructor(private events: Events) {
    }
    getUser() {
        this._user = this._getUser();
        return this._user;
    }
    getAuthorName() {
        this._user = this._getUser();
        return `${this._user.name} <${this._user.email}>`;
    }
    private _getUser() {
        try {
            var sus = localStorage.getItem('currentUser');
            if (sus == null) return null;
            var us = <User>JSON.parse(sus);
            return us
        }
        catch (ex) {
            return null;
        }
    }
    
}