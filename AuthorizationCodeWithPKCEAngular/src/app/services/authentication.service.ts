﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Events } from './events';
import { Router } from '@angular/router';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc-codeflow-pkce';
import { authConfig } from '../auth.config';
declare var require: any

export class User {
    public uid: string;
    public name: string;
    public email: string;
    public photo?: string;
    public token: string;
    public roles?: string[];
}

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private jwtDecode = require('jwt-decode');

    constructor(private http: HttpClient, private events: Events, private router: Router, private oauthService: OAuthService) { }

    initAuthorizationCodeFlow() {
        const crypto = require('crypto');
        var base64URLEncode = (str) => {
            return str.toString('base64')
                .replace(/\+/g, '-')
                .replace(/\//g, '_')
                .replace(/=/g, '');
        }
        var codeVerifier = base64URLEncode(crypto.randomBytes(32));
        console.log("code_verifier: " + codeVerifier);
        localStorage.setItem('__codeVerifier', codeVerifier);

        var encrypt = (buffer) => {
            return crypto.createHash('sha256').update(buffer).digest();
        }
        var code_challenge = base64URLEncode(encrypt(codeVerifier));
        console.log("Scode: " + code_challenge);

        this.oauthService.configure({
            clientId: authConfig.clientId,
            issuer: `${authConfig.issuer}/token`,
            redirectUri: authConfig.redirectUri,
            scope: authConfig.scope,
            tokenEndpoint: `${authConfig.issuer}/token`,
            userinfoEndpoint: `${authConfig.issuer}/userinfo`,
            loginUrl: `${authConfig.issuer}/authorize`,
            showDebugInformation: true,
            requireHttps: false,
            responseType: 'code',
            customQueryParams: {
                code_challenge: code_challenge,
                code_challenge_method: "S256"
            },
            strictDiscoveryDocumentValidation: false,
            oidc: true,
            disableAtHashCheck: true
        });
        this.oauthService.tokenValidationHandler = new JwksValidationHandler();
        this.oauthService.tryLogin({});
        this.oauthService.initAuthorizationCodeFlow();
    }


    processAutorizationCode(code: string) {
        var self = this;
        if (code == null || code.length == 0)
            return this.redirectLogin();

        var codeVerifier = localStorage.getItem('__codeVerifier');

        if (codeVerifier == null || codeVerifier.length == 0)
            return this.redirectLogin();

        localStorage.setItem('__codeVerifier', null);
        const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var url = `${authConfig.issuer}/token`;

        let body = `grant_type=authorization_code&client_id=${authConfig.clientId}&redirect_uri=${authConfig.redirectUri}&code_verifier=${codeVerifier}&code=${code}`;

        self.http.post(url, body, { headers: headers }).toPromise().then((res) => {
            console.log("Token: " + res);
            self.onToken(res);
        }).catch((reason) => {
            console.error(reason);
            return this.redirectLogin();
        });
    }


    logout(reload?) {
        console.log("logout");
        localStorage.removeItem('currentUser');
        this.events.broadcast("onUserStateChanged", null);
        if (reload) document.location.reload(false);
    }


    logoutWithRevokeToken() {
        this.revokeToken();
        this.logout(true);
    }

    logoutWithCleanCookie() {
        this.deleteCookie("");
        this.logout(true);
    }


    private revokeToken() {
        var self = this;
        const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var url = `${authConfig.issuer}/revoke`;

        var us = localStorage.getItem('currentUser');
        if(us == null) return;
        var user = <User>JSON.parse(us);
        if(user == null) return;
        let body = `token_type_hint=access_token&token=${user.token}&client_id=${authConfig.clientId}`;
        self.http.post(url, body, { headers: headers }).toPromise().then((res) => {
            console.log("Revoke OK");
        }).catch((reason) => {
            console.log("Revoke FAIL");
        });
    }

    private deleteCookie(name) {
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

    private onToken(res) {
        if (res == null || res.id_token == null || res.access_token == null)
            return this.redirectLogin();
        var decoded = this.jwtDecode(res.id_token);
        /*
        if (decoded.sub == null || !(<string>decoded.sub).startsWith("LACAJA/")) {
            this.router.navigate(['/nopermission']);
            return;
        }
        */
        var user: User = {
            uid: decoded.sub,
            name: decoded.name,
            email: decoded.email,
            token: res.access_token,
            roles: decoded.groups.join("<br/>")
        };
        if (user && user.token) {
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.events.broadcast("onUserStateChanged", user);
        }
        this.redirectMain();
    }


    private redirectLogin() {
        this.router.navigate(['/login']);
    }


    private redirectMain() {
        this.router.navigate(['/main']);
    }

}