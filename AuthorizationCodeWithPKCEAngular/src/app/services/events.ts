import { filter, map } from 'rxjs/operators';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { Injectable } from '@angular/core';
export interface BroadcastEvent {
  key: any;
  data?: any;
}

@Injectable()
export class Events {
  private _eventBus: Subject<BroadcastEvent>;

  constructor() {
    this._eventBus = new Subject<BroadcastEvent>();
  }

  broadcast(key: any, data?: any) {
    this._eventBus.next({key, data});
  }

  on<T>(key: any): Observable<T> {
    return this._eventBus.asObservable().pipe(filter(event => event.key === key),map(event => <T>event.data));
  }
}
