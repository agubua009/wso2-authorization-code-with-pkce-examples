
export interface Filters {
    idTechnology: number;
    idBrand: number;
    instanceId: number;
    schemaName: string;
    idObjectType: number;
    objectName: string;
    objectNameType: string
}