import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { MainComponent } from './pages/main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuard } from './_guards/auth.guard';
import { NoPermissionsComponent } from './pages/nopermissions/nopermissions.component';
import { CallBackComponent } from './pages/callback/callback.component';
export const routes: Routes = [
    { path: '', component: MainComponent, canActivate: [AuthGuard] },
    { path: 'main', component: MainComponent, canActivate: [AuthGuard] },    
    { path: 'login', component: LoginComponent },
    { path: 'nopermission', component: NoPermissionsComponent },
    { path: 'callback', component: CallBackComponent },


    // otherwise redirect to home
    { path: '**', component: MainComponent, canActivate: [AuthGuard] },
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes/*, { useHash: true }*/);
